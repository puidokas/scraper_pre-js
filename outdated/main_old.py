import mechanize
import webbrowser
import os
import urlparse
import requests
import urllib
import urllib2

from bs4 import BeautifulSoup
import re

URL="http://findbolig_old.nu/logind.aspx"
headers={"User-Agent":'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'}

username=""
password=""

s=requests.Session()
s.headers.update(headers)
r=s.get(URL)
soup=BeautifulSoup(r.content, "html5lib")

VIEWSTATE=soup.find(id="__VIEWSTATE")['value']
VIEWSTATEGENERATOR=soup.find(id="__VIEWSTATEGENERATOR")['value']
EVENTVALIDATION=soup.find(id="__EVENTVALIDATION")['value']

login_data={"__EVENTTARGET":"ctl00$placeholdercontent_1$but_Login",
"__EVENTARGUMENT":"",
"ctl00$placeholdercontent_1$txt_UserName":username,
"ctl00$placeholdercontent_1$txt_Password":password,
"ctl00_placeholdercontent_1_but_LoginShadow":"Log In"}

r=s.post(URL, data=login_data)

r = s.get("http://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")

html = r.text.encode('utf-8')
path = os.path.abspath('temp.html')
url = 'file://' + path

with open(path, 'w') as f:
    f.write(html)

webbrowser.open(url,new=2)