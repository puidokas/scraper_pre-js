# -*- coding: utf-8 -*-
import cookielib
import mechanize
import os
import webbrowser
import requests
from bs4 import BeautifulSoup
from tabulate import tabulate
import math
from datetime import datetime
from multiprocessing.dummy import Pool as ThreadPool
from Building import Building

COOKIE = cookielib.CookieJar()
BUILDING_CURR = 1

class Methods:
    def __init__(self, username, password):
        global COOKIE
        COOKIE = self.login(username, password)

    def fetch_webpage(self, url):
        global COOKIE
        response = requests.get(url, cookies=COOKIE)
        page_text = response.text.encode('utf-8').decode('ascii', 'ignore')

        return page_text

    def print_building_list(self, building_list):
        html_table = '<link href="../bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">' + \
             '<table class="table table-hover table-condensed table-bordered text-center">' + \
             '<thead><tr>' \
             '<th class="text-center">Building</th>' \
             '<th class="text-center">Name</th>' \
             '<th class="text-center">Company</th>' \
             '<th class="text-center">Location</th>' \
             '<th class="text-center">Apartments</th>' \
             '<th class="text-center">Waiting List Location</th></tr></thead><tbody>'

        for building in building_list:
            html_table = html_table + '<tr><td>' + building.img_url + '</td>' + \
            '<td>' + building.name + '</td>' + \
            '<td>' + building.company + '</td>' + \
            '<td>' + building.location + '</td>' + \
            '<td>' + building.apartments + '</td>' + \
            '<td>' + str(building.wlist_loc) + '</td></tr>'

        html_table = html_table + '</tbody></table>'

        self.save_report(html_table)

    def login(self, username, password):
        br = mechanize.Browser()
        cj = cookielib.CookieJar()
        br.set_cookiejar(cj)

        br.addheaders = [('User-agent',
                          'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36')]

        br.open("http://findbolig_old.nu/logind.aspx")
        br.select_form(nr=0)
        br.set_all_readonly(False)

        br["__EVENTTARGET"] = "ctl00$placeholdercontent_1$but_Login"
        br["__EVENTARGUMENT"] = ""
        br["ctl00$placeholdercontent_1$txt_UserName"] = username
        br["ctl00$placeholdercontent_1$txt_Password"] = password
        br.submit()

        return cj

    def off_get_registered_buildings(self):
        page_text = self.fetch_webpage("http://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")
        soup = BeautifulSoup(page_text, "html5lib")

        building_list_preextracted = soup.findAll("tr", class_="rowstyle")
        number_of_buildings = str(len(building_list_preextracted))
        building_list = []

        for building_information_preextracted in building_list_preextracted:
            building_information = self.extract_building_information(str(building_information_preextracted))
            building_list.append(building_information)

        pool = ThreadPool(12)
        building_list = pool.map(self.get_waiting_list_place, building_list)

        pool.close()
        pool.join()

        building_list = sorted(building_list, key=lambda x: x[5])
        return building_list

    def get_registered_buildings(self):
        page_text = self.fetch_webpage("http://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")
        soup = BeautifulSoup(page_text, "html5lib")

        building_list_overview = soup.findAll("tr", class_="rowstyle")
        building_list = []
        i = 0
        for building_list_row in building_list_overview:
            if i < 2:
                building = Building()
                building.id = str(building_list_row).partition("bid=")[2].partition('"')[0]

                building.apartments = str(building_list_row).partition('100px;">')[2].partition('</td>')[0]

                building.img_url = str(building_list_row).partition('src="')[2].partition('"')[0]
                building.img_url = '<a href="http://findbolig_old.nu/Ejendomspraesentation.aspx?bid=' + building.id + \
                                   '" target="_blank"><img src="http://findbolig_old.nu' + building.img_url + '"></a>'

                building_list.append(building)
                i += 1
            else:
                break

        print "Found " + str(len(building_list)) + " buildings!"

        pool = ThreadPool(12)
        building_list = pool.map(self.get_building_data, building_list)

        pool.close()
        pool.join()

        building_list = sorted(building_list, key=lambda x: x.wlist_loc)

        return building_list

    def get_building_data(self, building):
        page_text = self.fetch_webpage(
            "http://findbolig_old.nu/Ejendomspraesentation/Ejendommen.aspx?bid=" + building.id)
        soup = BeautifulSoup(page_text, "html5lib")

        building.name = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingNameTop").string

        building.location = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingAddressTop").string

        building.company = soup.find(id="ctl00_placeholdercontent_2_ImageOwner").get('src', '')
        building.company = '<img src="http://findbolig_old.nu' + building.company + '">'

        r = requests.post("http://findbolig_old.nu/Services/WaitlistService.asmx/GetWaitlistRank",
                          json={"buildingId": building.id}, cookies=COOKIE)
        building.wlist_loc = int(r.text.partition('nt":')[2].partition('}}')[0])

        building.print_data()
        return building

    def extract_building_information(self, building_information):
        bid = building_information.partition("bid=")[2].partition('"')[0]
        img = building_information.partition('src="')[2].partition('"')[0]
        img = '<a href="http://findbolig_old.nu/Ejendomspraesentation.aspx?bid=' + bid + '" target="_blank"><img src="http://findbolig_old.nu' + img + '"></a>'
        name = building_information.partition('0">')[2].partition('</f')[0]
        addr = building_information.rpartition('0">')[2].partition('</f')[0]
        bs = building_information.partition('100px;">')[2].partition('</td>')[0]

        return [int(bid), img, name, addr, bs]

    def get_waiting_list_place(self, building_information):
        global COOKIE
        global BUILDING_CURR
        bid = building_information[0]

        print "Getting waiting list place for building " + str(bid) + " (" + str(BUILDING_ANALYZED) + ")..."
        BUILDING_ANALYZED += 1
        r = requests.post("http://findbolig_old.nu/Services/WaitlistService.asmx/GetWaitlistRank", json={"buildingId": bid},
                          cookies=COOKIE)

        waiting_list_place = int(r.text.partition('nt":')[2].partition('}}')[0])
        building_information.append(waiting_list_place)

        return building_information

    def save_report(self, html):
        path = os.path.abspath('waiting_list_placements\\' + 'findbolig_old' + datetime.today().strftime("%y%m%d") + '.html')
        url = 'file://' + path

        with open(path, 'w') as f:
            f.write(html)

        webbrowser.open(url, new=2)

    def pull_unregistered_buildings_list(self, max_price):
        page_text = self.fetch_webpage("http://findbolig_old.nu/ventelisteboliger/liste.aspx?&rentmax=" + str(max_price) + "&roomsmin=2&showrented=1&showyouth=1&page=1&pagesize=100&sortdir=asc")
        soup = BeautifulSoup(page_text, "html5lib")

        pages = int(math.ceil(int(page_text.partition('ResidencesFoundTop">')[2].partition(' R')[0])/100.0))

        building_list = []

        for page in list(range(pages)):
            page_text = self.fetch_webpage("http://findbolig_old.nu/ventelisteboliger/liste.aspx?&rentmax=" + str(
                max_price) + "&roomsmin=2&showrented=1&showyouth=1&page=" + str(page+1) + "&pagesize=100&sortdir=asc")
            soup = BeautifulSoup(page_text, "html5lib")

            for building_information in soup.findAll("tr", class_="rowstyle"):
                building_link = "http://findbolig_old.nu" + str(building_information).partition('href="')[2].partition('"><img')[0]
                building_list.append(building_link)

        print "Search is complete."
        print "Found " + str(len(building_list)) + " buildings."

        return building_list

    def get_unregistered_buildings(self, building_list):
        unregistered_buildings = []
        count = 1
        building_list_total = str(len(building_list)+1)

        for building_link in building_list:
            print "(" + str(count) + "/" + building_list_total + ")" + "Checking, whether unregistered: " + building_link
            page_text = self.fetch_webpage(building_link)

            if "intSignedUpResidenceTable([]);" in page_text:
                print "Unregistered!"
                unregistered_buildings.append(building_link)

            count += 1

        return unregistered_buildings

