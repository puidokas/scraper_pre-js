import mechanize
import webbrowser
import os
import urlparse
import requests
import urllib
import urllib2
import cookielib

from bs4 import BeautifulSoup
import re

br = mechanize.Browser()
cj = cookielib.CookieJar()
br.set_cookiejar(cj)
br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36')]
response = br.open("http://findbolig_old.nu/logind.aspx")
br.select_form(nr=0)
br.set_all_readonly(False)
#print br.form

br["__EVENTTARGET"] = "ctl00$placeholdercontent_1$but_Login"
br["__EVENTARGUMENT"] = ""
br["ctl00$placeholdercontent_1$txt_UserName"] = ""
br["ctl00$placeholdercontent_1$txt_Password"] = ""

response = br.submit()
#print response.read()

response = requests.get("http://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?", cookies=cj)
# response = br.open("http://findbolig.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")

html = response.text.encode("utf8")
path = os.path.abspath('temp.html')
url = 'file://' + path

with open(path, 'w') as f:
    f.write(html)

webbrowser.open(url,new=2)

soup = BeautifulSoup(html, "html5lib")
for link in soup.findAll('a'):
    if "display=mysubs" in link.get('href'):
        bid = link.get('href').split('=')[1][:-8]
        #print link.get('href').split('=')[1][:-8]
        break

print 'Our bid is: ' + bid

r = requests.post("http://findbolig_old.nu/Services/WaitlistService.asmx/GetWaitlistRank", json={"buildingId":bid}, cookies=cj)

print r.content