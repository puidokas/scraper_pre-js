from bs4 import BeautifulSoup
from Building import Building
from multiprocessing.dummy import Pool as ThreadPool
import requests
from datetime import datetime
import helpers

BUILDING_CURR = 1
BUILDINGS_TOTAL = 0

def print_building_list(building_list):
    html_table = '<link href="../bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">' + \
                 '<table class="table table-hover table-condensed table-bordered text-center">' + \
                 '<thead><tr>' \
                 '<th class="text-center">Building</th>' \
                 '<th class="text-center">Name</th>' \
                 '<th class="text-center">Company</th>' \
                 '<th class="text-center">Location</th>' \
                 '<th class="text-center">Apartments</th>' \
                 '<th class="text-center">Waiting List Location</th></tr></thead><tbody>'

    for building in building_list:
        html_table = html_table + '<tr><td>' + building.img_url + '</td>' + \
                     '<td>' + building.name + '</td>' + \
                     '<td>' + building.company + '</td>' + \
                     '<td>' + building.location + '</td>' + \
                     '<td>' + building.apartments + '</td>' + \
                     '<td>' + str(building.wlist_loc) + '</td></tr>'

    html_table = html_table + '</tbody></table>'

    helpers.save_report(html_table)


def get_registered_buildings(self):
    page_text = helpers.fetch_webpage("https://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")
    soup = BeautifulSoup(page_text, "html.parser")

    building_list_overview = soup.findAll("tr", class_="rowstyle")
    building_list = []

    for building_list_row in building_list_overview:
        building = Building()
        building.id = str(building_list_row).partition("bid=")[2].partition('"')[0]

        building.apartments = str(building_list_row).partition('100px;">')[2].partition('</td>')[0]

        building.img_url = str(building_list_row).partition('src="')[2].partition('"')[0]
        building.img_url = '<a href="https://findbolig_old.nu/Ejendomspraesentation.aspx?bid=' + building.id + \
                           '" target="_blank"><img src="https://findbolig_old.nu' + building.img_url + '"></a>'

        building_list.append(building)

    buildings_total = len(building_list)
    print("Found " + str(len(building_list)) + " buildings!")

    pool = ThreadPool(12)
    building_list = pool.map(self.get_building_data, building_list)

    pool.close()
    pool.join()

    building_list = sorted(building_list, key=lambda x: x.wlist_loc)

    return building_list


def get_building_data(building, cookies, timeout):
    global BUILDING_CURR
    page_text = helpers.fetch_webpage(
        "https://findbolig_old.nu/Ejendomspraesentation/Ejendommen.aspx?bid=" + str(building.id))
    soup = BeautifulSoup(page_text, "html.parser")

    building.name = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingNameTop").string

    building.location = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingAddressTop").string

    building.company = soup.find(id="ctl00_placeholdercontent_2_ImageOwner").get('src', '')
    building.company = '<img src="https://findbolig_old.nu' + building.company + '">'

    while True:
        try:
            r = requests.post("https://findbolig_old.nu/Services/WaitlistService.asmx/GetWaitlistRank",
                              json={"buildingId": building.id}, cookies=cookies, timeout=timeout)
            break
        except requests.RequestException:
            print(str(datetime.now()) + ": POST Timeout occured. Retrying...")

    building.wlist_loc = int(r.text.partition('nt":')[2].partition('}}')[0])

    print(str(datetime.now()) + ": " + str(BUILDING_CURR) + "/" + str(BUILDINGS_TOTAL))
    BUILDING_CURR += 1

    building.print_data()

    return building
