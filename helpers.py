from selenium import webdriver
import requests
from datetime import datetime
import webbrowser


def login(username, password):
    cookies = {}
    wd = webdriver.PhantomJS()
    wd.get("https://www.findbolig_old.nu/logind.aspx")
    wd.find_element_by_id("ctl00_placeholdercontent_1_txt_UserName").send_keys(username)
    wd.find_element_by_id("ctl00_placeholdercontent_1_txt_Password").send_keys(password)
    wd.find_element_by_id("ctl00_placeholdercontent_1_but_LoginShadow").click()

    for cookie in wd.get_cookies():
        if (cookie['name'] == "ASP.NET_SessionId") or (cookie['name'] == ".ASPXAUTH"):
            cookies[cookie['name']] = cookie["value"]

    if len(cookies) != 2:
        raise ValueError('Cookies were returned incorrectly.')

    return cookies


def fetch_webpage(url, timeout, cookies):
    while True:
        try:
            response = requests.get(url, cookies=cookies, timeout=timeout)
            break
        except requests.RequestException:
            print(str(datetime.now()) + ": GET Timeout occurred. Retrying...")

    page_text = response.text.encode('utf-8').decode('ascii', 'ignore')

    return page_text

    # def fetch_webpage_with_js(self, url):
    #     global COOKIE
    #     while True:
    #         try:
    #             response = requests.get(url, cookies=COOKIE, timeout=TIMEOUT)
    #             break
    #         except requests.RequestException:
    #             print(str(datetime.now()) + ": GET Timeout occured. Retrying...")
    #
    #     page_text = response.text.encode('utf-8').decode('ascii', 'ignore')
    #
    #     return page_text


def save_report(self, html):
    path = os.path.abspath('waiting_list_placements\\' + 'findbolig_old' + datetime.today().strftime("%y%m%d") + '.html')
    url = 'file://' + path

    with open(path, 'w') as f:
        f.write(html)

    webbrowser.open(url, new=2)